import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/gestures.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Unsplash Gallery',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Unsplash Gallery'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var count = 5;
  var descriptionList = [], fullDescription = [], urlList = [];
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    descriptionList = List.generate(count, (index) => 'default');
    fullDescription = List.generate(count, (index) => 'description');
    urlList = List.generate(count, (index) => '');
    super.initState();
    refreshList();
  }

  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 3));

    var accessKey = '896d4f52c589547b2134bd75ed48742db637fa51810b49b607e37e46ab2c0043', maxLength = 25;
    try {
      var url = Uri.parse('https://api.unsplash.com/photos/random/?client_id=$accessKey&count=$count');
      var data;

      var response = await http.get(url);
      data = await json.decode(response.body);

      for(int i = 0; i < count; i++) {
        if(data[i]['description'] == null) {
          descriptionList[i] = 'Image';
          fullDescription[i] = 'Image';
        } else {
          if(data[i]['description'].length > maxLength) {
            descriptionList[i] = data[i]['description'].substring(0, maxLength) + '...';
          } else {
            descriptionList[i] = data[i]['description'];
          }
          fullDescription[i] = data[i]['description'];
        }
        urlList[i] = data[i]['urls']['small'];
      }
    } catch(e) {
      print(e);
    }

    setState(() {
      descriptionList = descriptionList;
      urlList = urlList;
      fullDescription = fullDescription;
    });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: RefreshIndicator(
        key: refreshKey,
        child: ListView.builder(
          itemCount: descriptionList.length,
          itemBuilder: (context, i) => ListTile(
            title: Container (
              margin: EdgeInsets.only(top: 15),
              child: Column(
                children: <Widget>[
                  Image.network(urlList[i], width: 300, height: 300, fit: BoxFit.cover, errorBuilder: (
                      BuildContext context, Object exception, StackTrace stackTrace) {
                    return Image.asset('assets/icon/icon.png', width: 300, height: 300, fit: BoxFit.cover);
                  },
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: Text(descriptionList[i], style: TextStyle(fontSize: 20, color: Colors.blueAccent, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                  ),
                ],
              ),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SelectedImage(
                  description: descriptionList[i],
                  imageUrl: urlList[i],
                  fullDescription: fullDescription[i],
                )),
              );
            },
          ),
        ),
        onRefresh: refreshList,
      ),
    );
  }
}

class SelectedImage extends StatefulWidget {
  SelectedImage({Key key, this.description, this.imageUrl, this.fullDescription}) : super(key: key);
  final String description, imageUrl, fullDescription;

  @override
  _SelectedImageState createState() => _SelectedImageState();
}

class _SelectedImageState extends State<SelectedImage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(widget.description),
      ),
      body: Center(
        child: Container (
          margin: EdgeInsets.only(top: 15),
          alignment: Alignment.center,
          child: Column(
            children: <Widget>[
              Image.network(widget.imageUrl, width: 300, height: 300, fit: BoxFit.cover, errorBuilder: (
                  BuildContext context, Object exception, StackTrace stackTrace) {
                return Image.asset('assets/icon/icon.png', width: 300, height: 300, fit: BoxFit.cover);
              },
              ),
              Padding(
                padding: EdgeInsets.only(left: 20, right: 20, top: 5),
                child: Text(widget.fullDescription, style: TextStyle(fontSize: 20, color: Colors.blueAccent, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
              ),
            ],
          ),
        ),
      ),
    );
  }
}